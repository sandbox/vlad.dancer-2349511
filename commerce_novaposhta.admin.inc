<?php
/**
 * @file
 * Commerce NovaPoshta service settings form.
 */

/**
 * Commerce NovaPoshta service settings form.
 *
 * @return mixed
 *   Settings form.
 */
function commerce_novaposhta_service_edit_form($form, &$form_state, $shipping_service = array()) {
  $form = array(
    $shipping_service['name'] => array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="commerce_novaposhta_service_edit_form">',
      '#suffix' => '</div>',
    ),
  );
  $service_name = $shipping_service['name'];
  if ($service_name == 'commerce_novaposhta_fixed_service') {
    $form[$service_name]['commerce_novaposhta_fixed_service_base_price'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Base price'),
      '#default_value'  => variable_get('commerce_novaposhta_fixed_service_base_price', 0),
    );
  }
  else {
    $form[$service_name]['commerce_novaposhta_calculated_service_city'] = array(
      '#type'           => 'select',
      '#title'          => t("Sender's city."),
      '#options'        => array(0 => '- Any -') + _commerce_novaposhta_get_cities(),
      '#default_value'  => variable_get('commerce_novaposhta_calculated_service_city', 0),
      '#ajax' => array(
        'callback'  => '_commerce_novaposhta_sender_city_ajax_callback',
        'wrapper'   => 'commerce_novaposhta_service_edit_form',
      ),
    );
    if (!empty($form_state['values']['commerce_novaposhta_calculated_service_city'])) {
      $city = $form_state['values']['commerce_novaposhta_calculated_service_city'];
    }
    else {
      $city = variable_get('commerce_novaposhta_calculated_service_city', 0);
    }
    $form[$service_name]['commerce_novaposhta_calculated_service_warehouse'] = array(
      '#type'           => 'select',
      '#title'          => t("Sender's warehouse."),
      '#options'        => array(0 => '- Any -') + _commerce_novaposhta_get_warehouses($city),
      '#default_value'  => variable_get('commerce_novaposhta_calculated_service_warehouse', 0),
    );
  }
  return system_settings_form($form);
}

/**
 * Get warehouses list.
 *
 * @param string $city_id
 *   City ID(City Ref).
 *
 * @return mixed
 *   Warehouses data array.
 */
function _commerce_novaposhta_get_warehouses($city_id) {
  $select = db_select('novaposhta_warehouses', 'w');
  $select->fields('w', array('warehouse_id', 'warehouse_data'));
  $select->condition('city_id', $city_id);
  $warehouses = $select->execute()->fetchAllKeyed();
  return _commerce_novaposhta_warehouses_build_list($warehouses);
}

/**
 * Build warehouses list in a correct format.
 *
 * @param array $warehouses
 *   Warehouses data array.
 *
 * @return array
 *   Warehouses formatted array.
 */
function _commerce_novaposhta_warehouses_build_list($warehouses = array()) {
  foreach ($warehouses as &$data) {
    $data = unserialize($data);
    $data = $data['description'];
  }
  _commerce_novaposhta_ua_sort($warehouses);
  return $warehouses;
}

/**
 * AJAX-callback.
 */
function _commerce_novaposhta_sender_city_ajax_callback($form, &$form_state) {
  return $form['commerce_novaposhta_calculated_service'];
}

/**
 * Get cities data array.
 *
 * @return mixed
 *   Cities array.
 */
function _commerce_novaposhta_get_cities() {
  $select = db_select('novaposhta_cities', 'c');
  $select->fields('c', array('city_id', 'description'));
  $cities = $select->execute()->fetchAllKeyed();
  _commerce_novaposhta_ua_sort($cities);
  return $cities;
}
